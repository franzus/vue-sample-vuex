// const AuthPage = () => import(/* webpackChunkName: "public" */`./containers/AuthPage`);
// const AuthForm = () => import(/* webpackChunkName: "public" */`./components/AuthForm`);
const HomePage = () => import(/* webpackChunkName: "public" */`./containers/HomePage`);

import Home from './components/Home.vue';
import About from './components/About.vue';

export default [
  {
    path: '',
    name: 'home',
    component: HomePage,
    children: [
      {
        path: '/',
        name: 'Home',
        component: Home
      },
      {
        path: '/about',
        name: 'About',
        component: About
      }
    ]
  }
];