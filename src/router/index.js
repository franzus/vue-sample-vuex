import Vue from 'vue';
import VueRouter from 'vue-router';

import { routes as Public } from '../modules/Public';


Vue.use(VueRouter);

const routes = [
  ...Public
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;

// import Vue from 'vue';
// import Router from 'vue-router';

// import { Trans } from './shared/utils/TranslateMiddleware';

// import MainLayout from './shared/containers/MainLayout';


// Vue.use(Router);

// export default new Router(
//   {
//     mode: 'history',
//     routes: [
//       {
//         path: '/:lang',
//         component: MainLayout,
//         beforeEnter: Trans.routeMiddleware,
//         scrollBehavior (to, from, savedPosition) { // eslint-disable-line
//           // if (savedPosition) {
//           //   return savedPosition;
//           // }
//           return { x: 0, y: 0 };
//         },
//         children: [
         
//         ]
//       },
//       {
//         // Redirect user to supported lang version.
//         path: '*',
//         redirect () {
//           return Trans.getUserSupportedLang();
//         }
//       }
//     ]
//   }
// );